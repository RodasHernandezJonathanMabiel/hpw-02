obj_profesor1 = 
{
    "clave_prof" : "P123",
    "nombre" : "Pablo Ramirez"
}

obj_profesor2 = 
{
    "clave_prof" : "P124",
    "nombre" : "Luis Hernandez"
}

obj_profesor3 = 
{
    "clave_prof" : "P125",
    "nombre" : "Ana Perez"
}

obj_profesor4 = 
{
    "clave_prof" : "P126",
    "nombre" : "Jorge Garcia"
}

obj_materia1 =
{
    "clave_materia" : "POO-1",
    "nombre" : "Programacion Orientada a Objetos"
}

obj_materia2 =
{
    "clave_materia" : "RDS-1",
    "nombre" : "Redes de Computadoras"
}

obj_materia3 =
{
    "clave_materia" : "HPW-2",
    "nombre" : "Herramientas de Programacion Web"
}

obj_materia4 =
{
    "clave_materia" : "ABD-3",
    "nombre" : "Admon. de Base de Datos"
}

obj_materia5 =
{
    "clave_materia" : "TAP-4",
    "nombre" : "Topicos Avanzados de Programacion"
}

obj_materia6 =
{
    "clave_materia" : "IAR-1",
    "nombre" : "Inteligencia Artificial"
}

obj_materia7 =
{
    "clave_materia" : "PDD-3",
    "nombre" : "Patrones de Diseño"
}

obj_materia8 =
{
    "clave_materia" : "SOP-1",
    "nombre" : "Sistemas Operativos"
}

obj_horario1 = 
{
    "clave_horario" : "1",
    "hora_inicio" : "07:00",
    "hora_final" : "08:00",
    "materia" : obj_materia1["clave_materia"],
    "profesor" : obj_profesor1["clave_prof"],
    "grupo" : "1" 
 }

obj_horario2 = 
{
    "clave_horario" : "2",
    "hora_inicio" : "08:00",
    "hora_final" : "09:00",
    "materia" : obj_materia2["clave_materia"],
    "profesor" : obj_profesor2["clave_prof"],
    "grupo" : "1"
 }
 
 obj_horario3 = 
{
    "clave_horario" : "3",
    "hora_inicio" : "09:00",
    "hora_final" : "10:00",
    "materia" : obj_materia3["clave_materia"],
    "profesor" : obj_profesor3["clave_prof"],
    "grupo" : "1"
 }
 
 obj_horario4 = 
{
    "clave_horario" : "4",
    "hora_inicio" : "10:00",
    "hora_final" : "11:00",
    "materia" : obj_materia4["clave_materia"],
    "profesor" : obj_profesor4["clave_prof"],
    "grupo" : "1"
 }
 
 obj_horario5 = 
{
    "clave_horario" : "5",
    "hora_inicio" : "11:00",
    "hora_final" : "12:00",
    "materia" : obj_materia5["clave_materia"],
    "profesor" : obj_profesor1["clave_prof"],
    "grupo" : "1"
 }
 
 obj_horario6 = 
{
    "clave_horario" : "6",
    "hora_inicio" : "12:00",
    "hora_final" : "13:00",
    "materia" : obj_materia6["clave_materia"],
    "profesor" : obj_profesor2["clave_prof"],
    "grupo" : "1"
 }
 
 obj_horario7 = 
{
    "clave_horario" : "7",
    "hora_inicio" : "13:00",
    "hora_final" : "14:00",
    "materia" : obj_materia7["clave_materia"],
    "profesor" : obj_profesor3["clave_prof"],
    "grupo" : "1"
 }

obj_alumno =
{
    "no_ctrol" : "12345",
    "nombre" : "Raul Hernandez",
    "grupo" : "1"
}

obj_alumno1 =
{
    "no_ctrol" : "12346",
    "nombre" : "Esther Sanchez",
    "grupo" : "1"
}

obj_alumno2 =
{
    "no_ctrol" : "12347",
    "nombre" : "Julio Pineda",
    "grupo" : "1"
}

lista_alumnos = 
    [
        obj_alumno,
        obj_alumno1,
        obj_alumno2
    ];
    
lista_horarios = 
    [
        obj_horario1,
        obj_horario2,
        obj_horario3,
        obj_horario4,
        obj_horario5,
        obj_horario6,
        obj_horario7
    ]

lista_profesor =
    [
        obj_profesor1,
        obj_profesor2,
        obj_profesor3,
        obj_profesor4
    ]

lista_materias = 
    [
        obj_materia1,
        obj_materia2,
        obj_materia3,
        obj_materia4,
        obj_materia5,
        obj_materia6,
        obj_materia7,
        obj_materia8
    ]
    
 obj_grupo1 =
 {
     "clave_grupo" : "1",
     "nombre" : "A",
     "alumnos" : 
        [ 
            obj_alumno["no_ctrol"],
            obj_alumno1["no_ctrol"],
            obj_alumno2["no_ctrol"]
        ],
     "materias" : 
        [
            obj_materia1["clave_materia"],
            obj_materia2["clave_materia"],
            obj_materia3["clave_materia"],
            obj_materia4["clave_materia"],
            obj_materia5["clave_materia"],
            obj_materia6["clave_materia"],
            obj_materia7["clave_materia"],
        ],
     "profesores" :
        [
            obj_profesor1["clave_prof"],
            obj_profesor2["clave_prof"],
            obj_profesor3["clave_prof"],
            obj_profesor4["clave_prof"],
        ],
     "horarios" :
        [
            obj_horario1["clave_horario"],
            obj_horario2["clave_horario"],
            obj_horario3["clave_horario"],
            obj_horario4["clave_horario"],
            obj_horario5["clave_horario"],
            obj_horario6["clave_horario"],
            obj_horario7["clave_horario"],
        ]
 }


function imprimir_lista_alumnos( grupo )
{
    var alumnos = grupo["alumnos"];
    var nombre;
        
        console.log( "No Ctrol \tNombre" );
        for( var i = 0; i < alumnos.length; i++ )
        {   nombre = alumnos[i] + "\t\t";
            for( var j = 0; j < lista_alumnos.length; j++ )
                if( lista_alumnos[j]["no_ctrol"] == alumnos[i] )
                {   nombre += lista_alumnos[j]["nombre"];
                    console.log( nombre );
                    j = lista_alumnos.length;
                }
        }
}

imprimir_lista_alumnos( obj_grupo1 );

function imprimir_lista_horarios( grupo )
{
    var horarios = grupo["horarios"];
    var horario;
        
        console.log( "Hora\t \tProfesor\t \tMateria" );
        for( var i = 0; i < horarios.length; i++ )
        {   
            for( var j = 0; j < lista_horarios.length; j++ )
            {   if( lista_horarios[j]["clave_horario"] == horarios[i] )
                {   
                    horario = lista_horarios[j]["hora_inicio"] + " - " + lista_horarios[j]["hora_final"] + "\t";
                    //console.log( horario );
                    for( var k = 0; k < lista_profesor.length; k++ )
                    {   if( lista_profesor[k]["clave_prof"] == lista_horarios[j]["profesor"] )
                        {
                            horario += lista_profesor[k]["nombre"] + "\t";
                            //console.log( horario );
                            k = lista_profesor.length + 1;
                        }
                    }
                    for( k = 0; k < lista_materias.length; k++ )
                    {   if( lista_materias[k]["clave_materia"] == lista_horarios[j]["materia"] )
                        {
                            horario += lista_materias[k]["nombre"];
                            console.log( horario );
                            k = lista_materias.length + 1;
                        }
                    }
                    j= lista_horarios.length + 1;
                }
                
            }
        }
}

imprimir_lista_horarios( obj_grupo1 );

function buscar_alumno( grupo, alumno )
{
    var alumnos = grupo["alumnos"];
        
        for( var i = 0; i < alumnos.length; i++ )
        {   
            for( var j = 0; j < lista_alumnos.length; j++ )
               if( lista_alumnos[j]["no_ctrol"] == alumnos[i] )
                    if( lista_alumnos[j]["nombre"] == alumno ) 
                        return true;
        }
        return false;
}

buscar_alumno( obj_grupo1, "Juan" );

function buscar_profesor( grupo, profesor )
{
    var profesores = grupo["profesores"];
        
        for( var i = 0; i < profesores.length; i++ )
        {   
            for( var j = 0; j < lista_profesor.length; j++ )
               if( lista_profesor[j]["clave_prof"] == profesores[i] )
                    if( lista_profesor[j]["nombre"] == profesor ) 
                        return true;
        }
        return false;
}

buscar_profesor( obj_grupo1, "Juan" );

function buscar_materia( grupo, materia )
{
    var materias = grupo["materias"];
        
        for( var i = 0; i < materias.length; i++ )
        {   
            for( var j = 0; j < lista_materias.length; j++ )
               if( lista_materias[j]["clave_materia"] == materias[i] )
                    if( lista_materias[j]["nombre"] == materia ) 
                        return true;
        }
        return false;
}

buscar_materia( obj_grupo1, "Calculo" );

